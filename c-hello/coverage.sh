#!/bin/bash
BUILD_DIR=build/gnu-release
rm -rf ${BUILD_DIR}
cmake --preset=gnu-release-coverage
cmake --build --preset=gnu-release-coverage
ctest --preset=gnu-release-coverage
lcov -d . -o ${BUILD_DIR}/app.info -b . -c --exclude '*/test/*' --exclude '*/src/main/*' --exclude '*/third/*' --exclude '/usr/include/*'
genhtml ${BUILD_DIR}/app.info -o ${BUILD_DIR}/lcov -t 'c-hello 测试覆盖率报告'