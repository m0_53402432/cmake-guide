#ifndef _C_HELLO_CONFIGURATION_VERION_H_
#define _C_HELLO_CONFIGURATION_VERION_H_

#include "c-hello/configuration/configuration.h"

#define __C_HELLO_VERSION_MAJOR_COPY (C_HELLO_VERSION_MAJOR)
#define __C_HELLO_VERSION_MINOR_COPY (C_HELLO_VERSION_MINOR)
#define __C_HELLO_VERSION_PATCH_COPY (C_HELLO_VERSION_PATCH)

#define __C_HELLO_VERSION_STR(R) #R
#define __C_HELLO_VERSION_STR2(R) __C_HELLO_VERSION_STR(R)

int get_c_hello_version_major();
int get_c_hello_version_minor();
int get_c_hello_version_patch();

#endif
