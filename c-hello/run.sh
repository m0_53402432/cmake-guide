#!/bin/bash

rm -rf build/ninja-release
cmake --preset=ninja-release
cmake --build --preset=ninja-release
cd build/ninja-release/dist/bin && ./c-hello