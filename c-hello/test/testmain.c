#include <CUnit/Basic.h>
#include <CUnit/CUnit.h>
#include <CUnit/Console.h>
#include <CUnit/TestDB.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlog.h>

#include "test_configuration.h"

// 待测试方法
int add(int x, int y) {
    return x + y;
}

// 测试方法
void test_add() {
    CU_ASSERT(3 == add(1, 2));
}

// suite 初始化
int suite_success_init() {
    return 0;
}

// suite 清理
int suite_success_clean() {
    return 0;
}

// 测试组
CU_TestInfo testcase[] = {{"test_for_add:", test_add}, CU_TEST_INFO_NULL};

CU_TestInfo test_configuration_case[] = {{"test_get_c_hello_version_major:", test_get_c_hello_version_major},
                                         CU_TEST_INFO_NULL};

// 测试套件
CU_SuiteInfo suites[] = {
    {"testSuite1", suite_success_init, suite_success_clean, NULL, NULL, testcase},
    {"test_configuration_case", suite_success_init, suite_success_clean, NULL, NULL, test_configuration_case},
    CU_TEST_INFO_NULL};

int main(int argc, char** argv) {
    int rc;
    rc = dzlog_init("zlog.conf", "my_cat");

    if (rc) {
        printf("init zlog failed! rc:%d\n", rc);
        zlog_fini();
        exit(EXIT_FAILURE);
    }

    dzlog_info("c-console test start...");

    // test main
    if (CU_initialize_registry()) {
        fprintf(stderr, " Initialization of Test Registry failed.");
        exit(EXIT_FAILURE);
    } else {
        assert(NULL != CU_get_registry());
        assert(!CU_is_test_running());

        if (CUE_SUCCESS != CU_register_suites(suites)) {
            exit(EXIT_FAILURE);
        }

        CU_basic_set_mode(CU_BRM_VERBOSE);
        CU_basic_run_tests();

        CU_cleanup_registry();

        dzlog_info("c-console test end...");
        zlog_fini();

        return CU_get_error();
    }

    return 0;
}