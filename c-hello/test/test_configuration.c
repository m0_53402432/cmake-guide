#include "test_configuration.h"

#include <zlog.h>

#include "c-hello/configuration/version.h"

void test_get_c_hello_version_major() {
    dzlog_info("test configuration version major");
    int major = get_c_hello_version_major();
    CU_ASSERT(1 == major);
}