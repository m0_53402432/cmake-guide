#include <stdio.h>
#include <stdlib.h>
#include <zlog.h>

#include "c-hello/configuration/config.h"

int main() {
    int rc;
    rc = dzlog_init("../conf/zlog.conf", "my_cat");

    if (rc) {
        printf("init zlog failed! rc:%d\n", rc);
        zlog_fini();
        exit(EXIT_FAILURE);
    }

    dzlog_info("hello world!");
    dzlog_info("c hello version major is %d", get_c_hello_version_major());
    dzlog_info("c hello version minor is %d", get_c_hello_version_minor());
    dzlog_info("c hello version patch is %d", get_c_hello_version_patch());

    zlog_fini();

    return 0;
}