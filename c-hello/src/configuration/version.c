#include "c-hello/configuration/version.h"

#include <stdlib.h>

#include "c-hello/configuration/configuration.h"

int get_c_hello_version_major() {
    return atoi(__C_HELLO_VERSION_STR2(C_HELLO_VERSION_MAJOR));
}
int get_c_hello_version_minor() {
    return atoi(__C_HELLO_VERSION_STR2(C_HELLO_VERSION_MINOR));
}
int get_c_hello_version_patch() {
    return atoi(__C_HELLO_VERSION_STR2(C_HELLO_VERSION_PATCH));
}