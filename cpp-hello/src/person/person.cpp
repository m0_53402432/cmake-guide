#include "cpp-hello/person/person.h"

#include <fmt/core.h>
#include <spdlog/spdlog.h>

#include "cpp-hello/util/log_util.h"

Person::Person() {
    this->log = LogUtil::getLogger("person");
}

Person::Person(int age, std::string&& name) : m_age(age), m_name(std::move(name)) {
    this->log = LogUtil::getLogger("person");
}

std::string Person::sayHello() {
    SPDLOG_LOGGER_INFO(log, "person say hello");
    return "Hello World";
}