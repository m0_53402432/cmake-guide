#include <fmt/core.h>
#include <spdlog/common.h>

#include <iostream>
#include <memory>

#include "cpp-hello/configuration/version.h"
#include "cpp-hello/person/person.h"
#include "cpp-hello/util/log_util.h"

int main() {
    LogUtil::init(spdlog::level::info, "../logs/app.log");

    std::shared_ptr<spdlog::logger> log = LogUtil::getLogger("app");
    SPDLOG_LOGGER_INFO(log, "cpp hello");

    std::unique_ptr<Person> person = std::make_unique<Person>(23, "laolang");
    SPDLOG_LOGGER_INFO(log, fmt::format(person->sayHello()));
    SPDLOG_LOGGER_INFO(log, fmt::format(person->name()));
    SPDLOG_LOGGER_INFO(log, fmt::format("cpp hello version major is : {}", get_cpp_hello_version_major()));
    SPDLOG_LOGGER_INFO(log, fmt::format("cpp hello version major is : {}", get_cpp_hello_version_major()));
    SPDLOG_LOGGER_INFO(log, fmt::format("cpp hello version patch is : {}", get_cpp_hello_version_patch()));
    return 0;
}