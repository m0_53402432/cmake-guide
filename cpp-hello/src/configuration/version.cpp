#include "cpp-hello/configuration/version.h"

#include <cstdlib>

#include "cpp-hello/configuration/configuration.h"

int get_cpp_hello_version_major() {
    return static_cast<int>(strtol(CPP_HELLO_VERSION_STR2(CPP_HELLO_VERSION_MAJOR), nullptr, 10));
}
int get_cpp_hello_version_minor() {
    return static_cast<int>(strtol(CPP_HELLO_VERSION_STR2(CPP_HELLO_VERSION_MINOR), nullptr, 10));
}
int get_cpp_hello_version_patch() {
    return static_cast<int>(strtol(CPP_HELLO_VERSION_STR2(CPP_HELLO_VERSION_PATCH), nullptr, 10));
}