#pragma once

#define CPP_HELLO_VERSION_MAJOR_COPY (CPP_HELLO_VERSION_MAJOR)
#define CPP_HELLO_VERSION_MINOR_COPY (CPP_HELLO_VERSION_MINOR)
#define CPP_HELLO_VERSION_PATCH_COPY (CPP_HELLO_VERSION_PATCH)

#define CPP_HELLO_VERSION_STR(R) #R
#define CPP_HELLO_VERSION_STR2(R) CPP_HELLO_VERSION_STR(R)

int get_cpp_hello_version_major();
int get_cpp_hello_version_minor();
int get_cpp_hello_version_patch();
