/**
 * @file common_util.h
 * @author khl (xiaodaima2016@163.com)
 * @brief 通用工具类
 * @version 0.1
 * @date 2024-01-05
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include <string>

/**
 * @brief 通用工具类
 */
class CommonUtil {
public:
    /**
     * @brief Construct a new Common Util object
     */
    CommonUtil() = default;
    /**
     * @brief 获取版本
     * 
     * @return 版本号
     */
    static std::string getVersion();
};
