#!/bin/bash
# rm -rf cmake-build-release
# cmake -S . -B cmake-build-release -DENABLE_COVERAGE=ON -DCMAKE_BUILD_TYPE=Release 
# cmake --build cmake-build-release
# cd cmake-build-release/dist/test && ./cpp-hello_test
# cd ../../../
# lcov -d . -o cmake-build-release/app.info -b . -c --exclude '*/test/*' --exclude '*/src/main/*' --exclude '*/third/*' --exclude '/usr/include/*'
# genhtml cmake-build-release/app.info -o cmake-build-release/lcov -t 'cpp-hello 测试覆盖率报告'

BUILD_DIR=build/gnu-release-coverage
rm -rf ${BUILD_DIR}
cmake --preset=gnu-release-coverage
cmake --build --preset=gnu-release-coverage
ctest --preset=gnu-release-coverage
lcov -d . -o ${BUILD_DIR}/app.info -b . -c --exclude '*/test/*' --exclude '*/src/main/*' --exclude '*/third/*' --exclude '*/build/*' --exclude '/usr/include/*'
genhtml ${BUILD_DIR}/app.info -o ${BUILD_DIR}/lcov -t 'cpp-hello 测试覆盖率报告'