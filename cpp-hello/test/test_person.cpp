#include <iostream>
#include <list>
#include <memory>

#include "cpp-hello/person/person.h"
#include "doctest/doctest.h"

TEST_SUITE("test_person") {
    TEST_CASE("test_person_one") {
        std::unique_ptr<Person> person = std::make_unique<Person>(23, "laolang");
        CHECK_EQ(23, person->age());
    }
}