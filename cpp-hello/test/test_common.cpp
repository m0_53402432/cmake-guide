#include <fmt/core.h>

#include <iostream>
#include <list>
#include <memory>

#include "cpp-hello/util/log_util.h"
#include "doctest/doctest.h"

TEST_SUITE("test_common") {
    TEST_CASE("test_common_one") {
        std::shared_ptr<spdlog::logger> log = LogUtil::getLogger("test");
        SPDLOG_LOGGER_INFO(log, fmt::format("test common one"));
    }
}